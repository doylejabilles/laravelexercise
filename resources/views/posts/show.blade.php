@extends('layouts.app')

@section('content')
    <a href="/posts" class="btn btn-secondary">back</a>
    <h1>{{ $post->title }}</h1>
    <small>Written on {{ $post->created_at }}</small>
    <hr>
        <img src="/storage/cover_images/{{$post->cover_image}}"/>
    
    <div>
        <!-- below is to parse HTML tags -->
        {!! $post->body !!}
        <hr>
        @if( !Auth::guest() ) <!-- This would hide buttons below if not logged in -->
            @if( Auth::user()->id === $post->user_id ) <!-- These button would only if the creator of the post is logged -->
                <a href="/posts/{{$post->id}}/edit" class="btn btn-secondary">Edit</a>

                {!! Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'text-right']) !!}
                    {{Form::hidden('_method', 'DELETE')}}
                    {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                {!! Form::close() !!}
            @endif
        @endif
    </div>
@endsection
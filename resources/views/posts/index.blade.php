@extends('layouts.app')

@section('content')
    <h1>Posts</h1>
    @if( count($posts) > 0 ) 
        @foreach( $posts as $post )
        <div class="row">
            <div class="col-md-2 col-sm-2">
                <img style="width: 100%" src="/storage/cover_images/{{$post->cover_image}}"/>
            </div>
            <div class="col-md-10 col-md-10">
                <div class="card">
                    <div class="card-header">
                        <h4><a href="/posts/{{ $post->id }}">{{ $post->title }}</a></h4>
                        <!-- /* The query $post->user->name is being query on the user table, after the relationship in model is created */ -->
                        <small>Wirtten on {{ $post->created_at }} by {{ $post->user->name }}</small>
                    </div>
                    <div class="card-body">
                        <p>{!! $post->body !!}</p>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        {{ $posts->links() }}<!-- For pagination -->
    @else
        <p>No post found</p>
    @endif

@endsection
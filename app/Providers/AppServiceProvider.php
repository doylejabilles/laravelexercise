<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
//use Illuminate\Support\Facades\Schema; //Laravel 5.5 and below Added to adjust max string in migrating and to avoid error
use Illuminate\Database\Schema\Builder;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Trigger the schema
        //Schema::defaultStringLength(191); //Laravel 5.5 and below
        Builder::defaultStringLength(191); //Laravel 5.7
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

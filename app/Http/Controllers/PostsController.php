<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;//this is to include strage file 
use App\Post; //including Post model
use DB; //This is to use OLD MySQL query

class PostsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //This is to put restriction to pages that needs to be logged in
        $this->middleware('auth', ['except' => ['index','show']]); //2nd parameter is adding exception.
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$posts = DB::select('SELECT * FROM posts');
        //$posts = DB::select('SELECT * FROM posts ORDER BY title DESC');
        //$posts = Post::all();
        //$posts = Post::orderBy('title','dec')->get();
        $posts = Post::orderBy('created_at','desc')->paginate(10); //for pagination
        return view('posts.index')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //calling in the validation
        $this->validate($request, [
            'title' => 'required',
            'body'  => 'required',
            'cover_image' => 'image|mimes:jpg,jpeg,png|nullable|max:1999'
        ]);

        //Handle file upload
        if( $request->hasFile('cover_image') ) {
            //file name with extension

            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();
            
            //Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            //Get just the extension
            $extension = $request->file('cover_image')->getClientOriginalExtension();

            //filename to store
            $filenameToStore = $filename . '_' . time() . '.'.$extension;

            //Upload image
            $path = $request->file('cover_image')->storeAs('public/cover_images', $filenameToStore);
        }

        //create post
        // Same approach using CLI through tinker
        $post = new Post;
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = auth()->user()->id; //getting currently logged in user
        ( $request->hasFile('cover_image') ) ? $post->cover_image = $filenameToStore : $post->cover_image = 'noimage.jpg';
        $post->save();

        //First Parameter of the with is the session name
        //Second parameter of the with is the message
        return redirect('/posts')->with('success','Post Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post =  Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        if( auth()->user()->id !== $post->user_id ) {
            return redirect('/posts')->with('error', 'Unauthorized page');
        }
        return view('posts.edit')->with('post',$post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'image|mimes:jpg,jpeg,png|nullable|max:1999',
        ]);

        if( $request->hasFile('cover_image') ) {

            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();
            $fileName = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            $fileNameToStore = $fileName . '_' . time() . '_' . $extension;
            $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);

        }

        $post = Post::find($id);
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        ( $request->hasFile('cover_image') ) ? $post->cover_image = $fileNameToStore : '';
        $post->save();

        return redirect('/posts/'.$id)->with('success','Updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        if( auth()->user()->id !== $post->user_id ) {
            return redirect('/posts')->width('error', 'Unauthorized Access');
        }

        if( $post->cover_image !== 'noimage.jpg' ) {
            Storage::delete('/storage/cover_images/'.$post->cover_image);
        }

        $post->delete();
        return redirect('/posts')->with('success','Post Deleted');
    }
}

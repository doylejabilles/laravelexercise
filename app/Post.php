<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //table name
    protected $table = 'posts';

    public $primaryKey = 'id';
    public $timeStamp = true;
    
    //creating relationship to database
    public function user() {
        return $this->belongsTo('App\User'); //App/User is pointing to User.php(user model)
    }
}
